package sn.mansour.demoecommerce.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import sn.mansour.demoecommerce.Entities.Client;

@CrossOrigin("*")
@RepositoryRestResource   // crée le ws
public interface ClientRepository extends JpaRepository<Client, Long> {

    @RestResource(path = "/byNom")
    public Page<Client> findByNomContains(@Param("mc") String nom, Pageable pageable);


}

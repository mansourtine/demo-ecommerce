package sn.mansour.demoecommerce.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import sn.mansour.demoecommerce.Entities.Commande;

@CrossOrigin
@RepositoryRestResource
public interface CommandeRepository extends JpaRepository<Commande, Long> {

    @RestResource(path = "/byLibelle")
    public Page<Commande> findByLibelleContains(@Param("mc") String libelle, Pageable pageable);


}

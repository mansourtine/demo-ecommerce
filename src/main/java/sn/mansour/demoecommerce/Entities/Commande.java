package sn.mansour.demoecommerce.Entities;

import javax.persistence.*;
import java.io.Serializable;
//import java.sql.Date;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "commande")
public class Commande implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double totPrice;
    private String libelle;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @OneToOne(optional=false,cascade=CascadeType.ALL, mappedBy="commande")
    private Facture facture;

    @ManyToOne(optional = false)
    private Client client;

    @ManyToMany(fetch = FetchType.EAGER)
    //@JoinTable(name = "detail_commande")
    private List<Produit> produitList;

    // Constructors
    public Commande() {
    }

    public Commande(double totPrice, String libelle, Date date, Facture facture, Client client, List<Produit> produitList) {
        this.totPrice = totPrice;
        this.libelle = libelle;
        this.date = date;
        this.facture = facture;
        this.client = client;
        this.produitList = produitList;
    }

    // Getters & Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getTotPrice() {
        return totPrice;
    }

    public void setTotPrice(double totPrice) {
        this.totPrice = totPrice;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Facture getFacture() {
        return facture;
    }

    public void setFacture(Facture facture) {
        this.facture = facture;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Produit> getProduitList() {
        return produitList;
    }

    public void setProduitList(List<Produit> produitList) {
        this.produitList = produitList;
    }
}

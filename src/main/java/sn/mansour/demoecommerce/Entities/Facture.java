package sn.mansour.demoecommerce.Entities;

import javax.persistence.*;
import java.io.Serializable;
//import java.sql.Date;
import java.util.Date;

@Entity
@Table(name = "facture")
public class Facture implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    private double montant;

    @OneToOne(optional = false)
    private Commande commande;


    // Constructors
    public Facture() {
    }

    public Facture(Date date, double montant, Commande commande) {
        this.date = date;
        this.montant = montant;
        this.commande = commande;
    }

    // Getters & Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }
}

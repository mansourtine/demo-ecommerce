package sn.mansour.demoecommerce.Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table
public class Produit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String label;
    private String description;
    private double prix;

    @ManyToMany(mappedBy = "produitList", fetch = FetchType.EAGER)
    private List<Commande> commandeList;

    // Constructors
    public Produit() {
    }

    public Produit(String label, String description, double prix, List<Commande> commandeList) {
        this.label = label;
        this.description = description;
        this.prix = prix;
        this.commandeList = commandeList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public List<Commande> getCommandeList() {
        return commandeList;
    }

    public void setCommandeList(List<Commande> commandeList) {
        this.commandeList = commandeList;
    }
}
